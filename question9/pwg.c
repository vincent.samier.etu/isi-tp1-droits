#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

struct usr_info {
    uid_t userId;
    char *passwd;
};

int addNewUser(FILE* f, struct usr_info *usr, char* newPass) {
    char buffer[70];
    fseek(f, 0, SEEK_END);
    sprintf(buffer, "%d", usr->userId);
    fputs(buffer, f);
    fputs(";", f);
    int res = fputs(crypt(newPass, "UN SALT CONSTANT, IDENTIQUE POUR TOUT LE MONDE"), f);
    if (res == EOF) {
        printf("Impossible d'ajouter un nouvel utilisateur");
        exit(EXIT_FAILURE);
    }
    return res;
}

FILE *openFile() {
    FILE *f = malloc(sizeof(FILE*));
    f = fopen("/home/admini/passwd", "r+");
    if (f == NULL) {
        perror("Cannot open file passwd");
        exit(EXIT_FAILURE);
    }
    return f;
}

FILE *createTempFile() {
    FILE *f = malloc(sizeof(FILE*));
    f = fopen("/home/admini/passwd.tmp", "w+");
    if (f == NULL) {
        perror("Cannot open/create file passwd");
        exit(EXIT_FAILURE);
    }
    return f;
}

void filecpy(FILE *src, FILE *dst, struct usr_info* usr) {
    fseek(src, 0, SEEK_SET);
    uid_t uid;
    char buffer[70];
    char* pass;
    struct usr_info cpy;
    cpy.passwd = malloc(sizeof(char[30]));
    while ((fgets(buffer, 70, src) > 0)) {
        uid = atoi(strtok(buffer, ";"));
        pass = strtok(NULL, ";");
        cpy.userId = uid;
        strcpy(cpy.passwd, pass);
        if (!(uid == usr->userId)) {
            addNewUser(dst, &cpy, cpy.passwd);
        }
    }
    free(cpy.passwd);
    return;
}

int findUserAndPass(FILE* f, struct usr_info* usr) {
    uid_t uid;
    char buffer[70];
    char* pass;
    while ((fgets(buffer, 70, f) > 0)) {
        uid = atoi(strtok(buffer, ";"));
        pass = strtok(NULL, ";");
        if (uid == usr->userId) {
            strcpy(usr->passwd, pass);
            return ftell(f);
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{
    struct usr_info usr;
    usr.userId = getuid();
    usr.passwd = malloc(sizeof(char[30]));
    FILE *f = NULL;
    f = openFile();
    char passwd[30];
    printf("Entrez votre mot de passe");
    fgets(passwd, 30, stdin);
    int found = findUserAndPass(f, &usr);

    if (found) {
        printf("User déjà enregistré");
        //On a trouvé un utilisateur avec son mot de passe
        if (strcmp(crypt(passwd, "UN SALT CONSTANT, IDENTIQUE POUR TOUT LE MONDE"), usr.passwd) == 0) {
            printf("Entrez votre nouveau mot de passe");
            fgets(passwd, 30, stdin);
            FILE *tmp = createTempFile();
            filecpy(f, tmp, &usr);
            addNewUser(tmp, &usr, passwd);
            fclose(f);
            fclose(tmp);
            remove("/home/admini/passwd");
            rename("/home/admini/passwd.tmp", "/home/admini/passwd");
            exit(EXIT_SUCCESS);
        } 
        else {
        printf("Mot de passe incorrect");
            exit(EXIT_FAILURE);
        } 
    }
    addNewUser(f, &usr, passwd);
        
    return 0;
}