#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    printf("EUID : %d, EGID : %d, RUID : %d, RGID : %d", geteuid(), getegid(), getuid(), getgid());
    fflush(stdout);
    FILE *f;
    f = fopen("mydata.txt", "r");
    if (f == NULL) {
        perror("Cannot open file mydata.txt");
        exit(EXIT_FAILURE);
    }
    char buffer[500];
    while (fgets(buffer, 500, f) > 0) {
        printf("%s", buffer);
    }
    return 0;
}
