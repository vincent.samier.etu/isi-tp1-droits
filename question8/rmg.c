#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

int userInFileGrp(int filegrp) {
    gid_t list[10];
    int nbgrp = getgroups(10, list);
    for (int i = 0; i<nbgrp; i++) {
        if (list[i] == filegrp)
            return 1;
    }
    return 0;
}

int logIn(char *passwd) {
    FILE *f;
    f = fopen("/home/admini/passwd", "r");
    if (f == NULL) {
        perror("Cannot open file passwd");
        exit(EXIT_FAILURE);
    }
    uid_t usr_id = getuid();
    char buffer[70];
    while ((fgets(buffer, 70, f) > 0)) {
        uid_t uid;
        char* pass;
        uid = atoi(strtok(buffer, ";"));
        pass = strtok(NULL, ";");
        if (uid == usr_id && (strcmp(crypt(passwd, "UN SALT CONSTANT, IDENTIQUE POUR TOUT LE MONDE"), pass) == 0))
            return 1;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Usage rmg <FileToDelete>");
        exit(EXIT_FAILURE);
    }
    struct stat st;
    int res = stat(argv[1], &st);
    if (res == -1) {
        printf("Impossible de trouver le fichier %s", argv[1]);
        exit(EXIT_FAILURE);
    }
    if (!userInFileGrp(st.st_gid)) {
        printf("Vous n'avez pas les droits pour supprimer ce fichier");
        exit(EXIT_FAILURE);
    }
    char passwd[30];
    printf("Entrez votre mot de passe");
    fgets(passwd, 30, stdin);
    if (!logIn(passwd))
    {
        printf("Mot de passe incorrect");
        exit(EXIT_FAILURE);
    }
    if (remove(argv[1])) {
        printf("Erreur de la suppression du fichier");
        exit(EXIT_FAILURE);
    }
    printf("Fichier supprimé :)");
    return 0;
}