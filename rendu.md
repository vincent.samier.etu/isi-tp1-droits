# Rendu "Les droits d’accès dans les systèmes UNIX"

## Monome

- Nom, Prénom, email: Samier, Vincent, vincent.samier.etu@univ-lille.fr

## Question 1

Le processus est lancé par Toto, donc son Euid est égal au Ruid de Toto.  
Le fichier appartient à Toto.  
Sur le fichier, Toto a le droit de lecture seulement.  
Le processus teste son Euid avec le Ruid du possesseur du fichier. Ils correspondent.  
Le processus utilise donc le premier triplet de permissions pour les droits.  
Donc sur ce fichier, le processus a seulement le droit de lecture.  
Le processus ne peut donc pas écrire sur ce fichier.  

## Question 2

Pour un répertoire, avoir le droit d'éxection veut dire qu'on peut accéder à ses sous répertoires.
Toto ne peut pas entrer dans le répertoire mydir, car il n'a pas le droit d'éxecution sur ce répertoire puisqu'il fait parti du groupe ubuntu.
La commande `ls -al mydir` fonctionne puisque toto a le droit de lecture sur le répertoire mydir. Cependant, il ne peut pas avoir d'informations sur les fichiers que le répertoire contient car il n'a pas les droits d'éxecution sur ce répertoire, il n'a donc pas accès aux fichiers.  

## Question 3
Résultat de la première éxecution:  
-> Cannot open file mydata.txt: Permission denied  
-> EUID : 1001, EGID : 1001, RUID : 1001, RGID : 1001  
Le processus n'arrive pas à ouvrir le fichier en lecture  

Résultat de la deuxième éxecution  
-> EUID : 1000, EGID : 1001, RUID : 1001, RGID : 1001  
Le processus arrive à ouvrir le fichier en lecture  

## Question 4

`python3 ./suid.py`
EUID :  1001
EGID :  1001
`id`
-> uid=1001(toto) gid=1001(toto) groups=1001(toto),1000(ubuntu)

Pour modifier un des attributs du fichier /etc/passwd, il suffit que l'utilisateur éxecute un script avec les privilèges de l'administrateur dans lequel il appelle par exemple chmod sur le ficher /etc/passwd.

## Question 5

`cat /etc/passwd`
-> toto:x:1001:1001::/home/toto:/bin/bash

`man chfn`
chfn - change real user name and information
chfn sert à changer les informations d'un utilisateur.

`ls -al /usr/bin/chfn`
-> -rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
Les informations d'un utilisateur sont stockées dans le fichier /etc/passwd qui appartient à root. Seul l'utilisateur root a le droit d'écriture sur ce fichier. Le fichier /usr/bin/chfn a le set-user-id d'activé pour qu'il puisse être éxecuté avec les droits de l'utilisateur root, sinon il serait impossible de modifier le fichier /etc/passwd. Tout le monde a le droit d'éxecution sur ce fichier, afin que n'importe qui puisse modifier ses informations.

`cat /etc/passwd`
-> toto:x:1001:1001:,1,1,1:/home/toto:/bin/bash
Les informations ont bien été mis à jour


## Question 6

Les mots de passe des utilisateurs sont stockés dans le fichier /etc/shadow.
Comme tout le monde a le droit de lecture sur le fichier /etc/passwd, on ne peut raisonnablement pas y stocker des mots de passe.
Ils sont donc stockés dans un autre fichier, accessible seulement par l'utilisateur root et les membres du groupe shadow.

## Question 7


## Question 8

Pour ces questions, le répertoire admin est remplacé par le répertoire admini, puisqu'un répertoire admin existe déjà, et appartient au groupe admin. J'ai supposé que l'admin du système ne devait pas avoir le même groupe que l'admin du serveur partagé.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








