#!/bin/bash

su
addgroup groupe_a
addgroup groupe_b
addgroup shared
adduser ua
adduser ua groupe_a
adduser ua shared

adduser ub
adduser ub groupe_b
adduser ub shared

adduser admini
adduser admini groupe_a
adduser admini groupe_b
adduser admini shared

su admini
mkdir serveur
cd serveur
mkdir dir_a
mkdir dir_b
mkdir dir_c
chgrp groupe_a dir_a
chgrp groupe_b dir_b
chgrp shared dir_c
chmod +t dir_a
chmod +t dir_b
chmod g+s dir_a
chmod g+s dir_b
chmod g+s dir_c

chmod 755 dir_c
chmod 771 dir_a
chmod 771 dir_b

touch dir_a/testfile
touch dir_b/testfile
touch dir_c/

su ua
rm dir_a/testfile
rm dir_b/testfile
rm dir_c/
touch dir_a/uafile
echo "aaaa" > dir_a/uafile
cat dir_a/uafile
mv dir_a/uafile dir_a/uafile2
mv dir_a/testfile dir_a/testfile2
rm dir_c/testfile

su ub
rm dir_b/testfile
rm dir_b/testfile
rm dir_c/
touch dir_b/uafile
echo "aaaa" > dir_b/uafile
cat dir_b/uafile
mv dir_b/uafile dir_b/uafile2
mv dir_b/testfile dir_b/testfile2
rm dir_c/testfile
